# README #

1/3スケールの初期マッキントッシュ風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_early_macintosh/raw/29061c6f3e5bfd10c31451070f848ebfebb9a262/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_early_macintosh/raw/29061c6f3e5bfd10c31451070f848ebfebb9a262/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_early_macintosh/raw/29061c6f3e5bfd10c31451070f848ebfebb9a262/ExampleImage.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_early_macintosh/raw/29061c6f3e5bfd10c31451070f848ebfebb9a262/ExampleImage_2.png)
